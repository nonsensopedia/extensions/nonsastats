CREATE TABLE IF NOT EXISTS /*_*/nonsastats (
  `ns_page_id` int(10) unsigned NOT NULL,
  `ns_day` int(8) NOT NULL,
  `ns_hits` int(10) NOT NULL default 0,
  PRIMARY KEY  (`ns_page_id`,`ns_day`)
) /*$wgDBTableOptions*/;
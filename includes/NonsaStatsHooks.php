<?php
/**
 * NonsaStats extension hooks
 *
 * @file
 * @ingroup Extensions
 * @license MIT
 */
class NonsaStatsHooks
{
    public static function onPageViewUpdates(WikiPage $wikipage, User $user)
    {
        if (!NonsaStats::canPageBeTracked($wikipage->getTitle()))
            return;

        NonsaStats::updatePageViews($wikipage->getTitle());
    }

    public static function onBeforePageDisplay(OutputPage $out, Skin $skin)
    {
        global $wgNStatsViewTrackingAllowedNamespaces;

        $out->addJsConfigVars([
            'wgNStatsViewTrackingAllowedNamespaces' => $wgNStatsViewTrackingAllowedNamespaces
        ]);
    }

    public static function onInfoAction(IContextSource $context, &$pageInfo)
    {
        if (!NonsaStats::canPageBeTracked($context->getTitle()))
            return;

        $pageViews = NonsaStats::getPageViews($context->getTitle());

        $pageInfo['header-basic'][] = [
            $context->msg('nonsaStats-view-count-label'),
            number_format($pageViews, 0, '', ' ')
        ];
    }

    /**
     * Adds the required table storing votes into the database when the
     * end-user (sysadmin) runs /maintenance/update.php
     *
     * @param DatabaseUpdater $updater
     */
    public static function onLoadExtensionSchemaUpdates($updater)
    {
        $patchPath = __DIR__ . '/../sql/';

        $updater->addExtensionTable(
            'nonsastats',
            $patchPath . 'create-table--nonsastats.sql'
        );
    }
}